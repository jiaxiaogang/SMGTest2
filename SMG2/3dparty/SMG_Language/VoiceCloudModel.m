//
//  VoiceCloudModel.m
//  SMG_Language
//
//  Created by 贾  on 2017/4/6.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "VoiceCloudModel.h"
#import "SMGLanguageHeader.h"

@implementation VoiceCloudModel

-(id) initWithDictionary:(NSDictionary*)dic{
    self = [super init];
    if (self && dic) {
        self.id = [STRTOOK([dic objectForKey:@"id"]) integerValue];
        self.cont = STRTOOK([dic objectForKey:@"cont"]);
        self.pos = STRTOOK([dic objectForKey:@"pos"]);
        self.ne = STRTOOK([dic objectForKey:@"ne"]);
        self.parent = [STRTOOK([dic objectForKey:@"parent"]) integerValue];
        self.relate = STRTOOK([dic objectForKey:@"relate"]);
        self.semparent = [STRTOOK([dic objectForKey:@"semparent"]) integerValue];
        self.semrelate = STRTOOK([dic objectForKey:@"semrelate"]);
        self.arg = ARRTOOK([dic objectForKey:@"arg"]);
    }
    return self;
}

/**
 *  MARK:--------------------NSCoding--------------------
 */
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.id = [aDecoder decodeIntegerForKey:@"id"];
        self.cont = [aDecoder decodeObjectForKey:@"cont"];
        self.pos = [aDecoder decodeObjectForKey:@"pos"];
        self.ne = [aDecoder decodeObjectForKey:@"ne"];
        self.parent = [aDecoder decodeIntegerForKey:@"parent"];
        self.relate = [aDecoder decodeObjectForKey:@"relate"];
        self.semparent = [aDecoder decodeIntegerForKey:@"semparent"];
        self.semrelate = [aDecoder decodeObjectForKey:@"semrelate"];
        self.arg = ARRTOOK([aDecoder decodeObjectForKey:@"arg"]);
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger:self.id forKey:@"id"];
    [aCoder encodeObject:self.cont forKey:@"cont"];
    [aCoder encodeObject:self.pos forKey:@"pos"];
    [aCoder encodeObject:self.ne forKey:@"ne"];
    [aCoder encodeInteger:self.parent forKey:@"parent"];
    [aCoder encodeObject:self.relate forKey:@"relate"];
    [aCoder encodeInteger:self.semparent forKey:@"semparent"];
    [aCoder encodeObject:self.semrelate forKey:@"semrelate"];
    [aCoder encodeObject:self.arg forKey:@"arg"];
}

@end
