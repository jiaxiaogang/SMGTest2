//
//  GetVoiceCloudApi.m
//  SMG_Language
//
//  Created by 贾  on 2017/3/20.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "GetVoiceCloudApi.h"
#import "SMGLanguageHeader.h"

@implementation GetVoiceCloudApi

- (NSString *)requestUrl {
    return @"/analysis/";
}

- (NSString *)baseUrl {
    return @"http://ltpapi.voicecloud.cn";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

/**
 *  MARK:--------------------参数集--------------------
 *  1,分词:
 *      URL:GET http://api.ltp-cloud.com/analysis/?api_key=YourApiKey&text=我是中国人。&pattern=ws&format=plain
 *      结果:(我 是 中国 人 。)
 *  2,词性标注:
 *      URL:GET http://api.ltp-cloud.com/analysis/?api_key=YourApiKey&text=我是中国人。&pattern=pos&format=plain
 *      结果:(我_r 是_v 中国_ns 人_n 。_wp)
 *  3,命名实体识别:
 *      URL:GET http://api.ltp-cloud.com/analysis/?api_key=YourApiKey&text=我是中国人。&pattern=ner&format=plain
 *      结果:(我 是 [中国]Ns 人 。)
 *  4,依存语法分析:
 *      URL:GET http://api.ltp-cloud.com/analysis/?api_key=YourApiKey&text=我是中国人。&pattern=dp&format=plain
 *      结果:
 *          (我_0 是_1 SBV
 *          是_1 -1 HED
 *          中国_2 人_3 ATT
 *          人_3 是_1 VOB
 *          。_4 是_1 WP)
 *  5,语义依存分析:
 *      URL:GET http://api.ltp-cloud.com/analysis/?api_key=YourApiKey&text=我是中国人。&pattern=sdp&format=plain
 *      结果:
 *          我_0 是_1 Exp
 *          是_1 -1 Root
 *          中国_2 人_3 Nmod
 *          人_3 是_1 Clas
 *          。_4 是_1 mPunc
 *  6,语义角色标注:
 *      URL:GET http://api.ltp-cloud.com/analysis/?api_key=YourApiKey&text=我是中国人。&pattern=srl&format=plain
 *      结果:([我]A0 [是]v [中国 人]A1 。)
 *  7,XML:
 *      URL:GET http://api.ltp-cloud.com/analysis/?api_key=YourApiKey&text=我们都是中国人。&pattern=all&format=xml
 *      结果:...
 *  8,JSON:
 *      URL:GET http://api.ltp-cloud.com/analysis/?api_key=YourApiKey&text=我是中国人。&pattern=all&format=json
 *      结果:
 *
 */
- (id)requestArgument {
    NSMutableDictionary *argument = [[NSMutableDictionary alloc] init];
    [argument setObject:@"W1e4A9c0T0h0P2h048829BKH6VVvVPVVHyLoHHsm" forKey:@"api_key"];
    [argument setObject:[NSString stringWithFormat:@"%@",self.text] forKey:@"text"];
    [argument setObject:@"all" forKey:@"pattern"];
    [argument setObject:@"json" forKey:@"format"];
    
    return argument;
}

- (void)startWithCompletionBlockWithSuccess:(void(^)(GetVoiceCloudApi *request,NSMutableArray *modelArr))success{
    [self startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSMutableArray *mArr = [[NSMutableArray alloc] init];
        if (request.responseObject) {
            NSLog(@"%@",request.responseObject);
            NSArray *responseArr = ARRTOOK(request.responseObject);
            NSArray *parentArr = ARR_INDEX(responseArr, 0);
            NSArray *dataArr = ARR_INDEX(parentArr, 0);
            if (dataArr) {
                for (NSDictionary *dic in dataArr) {
                    VoiceCloudModel *model = [[VoiceCloudModel alloc] initWithDictionary:dic];
                    [mArr addObject:model];
                }
            }
        }
        if (success) {
            success(request,mArr);
        }
    } failure:nil];
}

@end
