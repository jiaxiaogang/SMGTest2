//
//  GetVoiceCloudApi.h
//  SMG_Language
//
//  Created by 贾  on 2017/3/20.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <YTKNetwork/YTKNetwork.h>

/**
 *  MARK:--------------------语音云--------------------
 *  讯飞:@"http://ltpapi.voicecloud.cn";
 *  LTP:@"http://api.ltp-cloud.com/analysis"
 */
@interface GetVoiceCloudApi : YTKRequest
@property (strong,nonatomic) NSString *text;
- (void)startWithCompletionBlockWithSuccess:(void(^)(GetVoiceCloudApi *request,NSMutableArray *modelArr))success;
@end
