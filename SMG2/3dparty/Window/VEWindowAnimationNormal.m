//
//  TempleteShadowWindow.m
//  XG_Window_IOS
//
//  Created by 贾  on 16/5/27.
//  Copyright © 2016年 XG_Window_IOS. All rights reserved.
//  https://www.github.com/jiaxiaogang/XG_Window_IOS
//

#import "VEWindowAnimationNormal.h"

@implementation VEWindowAnimationNormal

-(id) init{
    self = [super init];
    if (self) {
        self.duration_open = 0.3f;
        self.duration_close = 0.3f;
    }
    return self;
}

-(void) animation_Open:(UIView *)animationObj completion:(void (^)(BOOL))completion{
    [super animation_Open:animationObj completion:completion];
    if (animationObj) {
        //放大
        animationObj.transform = CGAffineTransformMakeScale(0.4, 0.4);
        [UIView animateWithDuration:(self.duration_open * 5.0f / 8.0f ) animations:^{
            animationObj.transform = CGAffineTransformMakeScale(1.05f, 1.05f);
        }completion:^(BOOL finished) {
            [UIView animateWithDuration:(self.duration_open * 3.0f / 8.0f ) animations:^{
                animationObj.transform = CGAffineTransformIdentity;
            }];
        }];
        
        //渐变
        [animationObj setAlpha:0];
        [UIView animateWithDuration:0.1f animations:^{
            animationObj.alpha = 1;
        }];
    }
}

-(void) animation_Close:(UIView *)animationObj completion:(void (^)(BOOL))completion{
    [super animation_Close:animationObj completion:completion];
    if (animationObj) {
        //放小
        [UIView animateWithDuration:self.duration_close animations:^{
            animationObj.transform = CGAffineTransformMakeScale(0.01f, 0.01f);
        }completion:^(BOOL finished) {
            if (completion) {
                completion(finished);
            }
        }];
        
        //渐变
        CGFloat duration_Alpha = 0.1f;
        if (self.duration_close < 0.1f) {
            if (self.duration_close > 0) {
                duration_Alpha = self.duration_close;
            }else{
                duration_Alpha = 0;
            }
        }
        [UIView animateWithDuration:duration_Alpha
                              delay:self.duration_close - duration_Alpha
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                            animationObj.alpha = 0;
                         } completion:^(BOOL finished) {}];
    }
}




@end
