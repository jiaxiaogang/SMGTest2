//
//  DB_Memory.m
//  SMG2
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "DB_Memory.h"

@implementation DB_Memory


static DB_Memory* db_Memory;

+(DB_Memory *)sharedInstance{
    if (!db_Memory){
        db_Memory = [[DB_Memory alloc]initWithDBName:@"Memory"];
    }
    return db_Memory;
}

/**
 *  MARK:--------------------DeepMemoryDao--------------------
 */
//-(BOOL) insertDeepMemory:(SMGMemoryModel*)model{
//    if (model &&
//        [model isKindOfClass:[SMGMemoryModel class]]) {
//        return [self insertToDB:model];
//    }
//    return true;
//}


@end
