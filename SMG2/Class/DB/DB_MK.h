//
//  DB_MK.h
//  SMG2
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <LKDBHelper/LKDBHelper.h>

@class MKFood;
@interface DB_MK : LKDBHelper

+(DB_MK *)sharedInstance;
@property (assign, nonatomic) NSInteger curMKObjectId;

/**
 *  MARK:--------------------MKFoodDao--------------------
 */
-(BOOL) insertMKFood:(MKFood*)model;
- (MKFood*)queryMKFood:(long long)objId;
- (NSArray*)queryMKFoodList;
@end
