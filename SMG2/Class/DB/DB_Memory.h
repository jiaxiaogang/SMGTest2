//
//  DB_Memory.h
//  SMG2
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <LKDBHelper/LKDBHelper.h>


/**
 *  MARK:--------------------统一写所有sqlStr语句在这--------------------
 */
@interface DB_Memory : LKDBHelper

+ (DB_Memory *)sharedInstance;

/**
 *  MARK:--------------------DeepMemoryDao--------------------
 */
//-(BOOL) insertDeepMemory:(SMGMemoryModel*)model;

@end
