//
//  DB_MK.m
//  SMG2
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "DB_MK.h"
#import "MKFood.h"

@implementation DB_MK


static DB_MK *db_MK;

+(DB_MK *)sharedInstance{
    if (!db_MK){
        db_MK = [[DB_MK alloc]initWithDBName:@"MappingKnowledge"];
    }
    return db_MK;
}

/**
 *  MARK:--------------------通用的父类insert方法--------------------
 *  递归保存父类直至MKObject基类;
 */
-(void) insertMKModel:(MKObject*)model{
    if (model && [model isKindOfClass:[MKObject class]]) {
        [self insertToDB:model];
    }
}

/**
 *  MARK:--------------------MKFoodDao--------------------
 */
-(BOOL) insertMKFood:(MKFood*)model{
    if (model &&
        [model isKindOfClass:[MKFood class]]) {
        return [self insertToDB:model];
    }
    return true;
}

- (MKFood*)queryMKFood:(long long)objId{
    
    NSString *where = [NSString stringWithFormat:@"objId = %lld", objId];
    MKFood *model = nil;
    NSMutableArray* searchResultArray = [MKFood searchWithWhere:where orderBy:nil offset:0 count:1];
    if (searchResultArray && [searchResultArray count]) {
        model = searchResultArray[0];
    }
    return model;
}

- (NSArray*)queryMKFoodList{
    NSMutableArray* searchResultArray = [MKFood searchWithWhere:nil orderBy:nil offset:0 count:100];
    return searchResultArray;
}
@end
