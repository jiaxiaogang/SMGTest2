//
//  WordConcrete.h
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WordBase.h"



/************************ AttributesKEY ************************/
/*UIKIT_EXTERN*/static NSString * const NSWordConcreteAttributeName_Hardness = @"NSWordConcreteAttributeName_Hardness"; //NSNumber containing CGFloat,default 0:
static NSString * const NSWordConcreteAttributeName_TtansferTeacher = @"NSWordConcreteAttributeName_TtansferTeacher";   //调教师
static NSString * const NSWordConcreteAttributeName_TrustValue = @"NSWordConcreteAttributeName_TrustValue";             //可信度

/**
 *  MARK:--------------------具象词汇--------------------
 */
@interface WordConcrete : WordBase

@property (strong,nonatomic) NSMutableDictionary *attributes;   //属性集

@end
