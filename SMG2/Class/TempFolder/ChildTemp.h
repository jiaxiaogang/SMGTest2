//
//  ChildTemp.h
//  Tanker
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 Tanker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTemp.h"

@interface ChildTemp : BaseTemp
@property (strong,nonatomic) NSString *childVar;
@end
