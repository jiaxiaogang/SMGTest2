//
//  BaseTemp.h
//  Tanker
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 Tanker. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseTemp : NSObject

@property (strong,nonatomic) NSString *baseVar;

-(void) getTemp;
@end
