//
//  FoodDemand.m
//  SMG2
//
//  Created by 贾  on 2017/3/20.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "FoodDemand.h"
#import "DemandUtils.h"
#import "XGUntil.h"

@implementation FoodDemand

-(CGFloat) getCurrentFoodDemandValue{
    CGFloat curBattery = [XGUntil getBatteryValue];
    return 100 - curBattery;
}

@end
