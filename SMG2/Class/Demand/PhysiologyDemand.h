//
//  PhysiologyDemand.h
//  SMG2
//
//  Created by 贾  on 2017/3/20.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "DemandBase.h"

/**
 *  MARK:--------------------生理需求--------------------
 *  依照:马斯洛需求层次理论
 *  子类:食物,水,空气,性欲,健康,睡眠,呼吸等
 */
@interface PhysiologyDemand : DemandBase

@end
