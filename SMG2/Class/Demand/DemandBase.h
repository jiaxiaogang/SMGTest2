//
//  DemandBase.h
//  SMG2
//
//  Created by 贾  on 2017/3/20.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  MARK:--------------------需求基类--------------------
 *  依照:马斯洛需求层次理论
 *  子类:生理需求,安全需求,社交需求,尊重需求,自我实现
 */
@interface DemandBase : NSObject

@end
