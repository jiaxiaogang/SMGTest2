//
//  FoodDemand.h
//  SMG2
//
//  Created by 贾  on 2017/3/20.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "PhysiologyDemand.h"

/**
 *  MARK:--------------------食欲--------------------
 */
@interface FoodDemand : PhysiologyDemand


/**
 *  MARK:--------------------当前食欲值--------------------
 *  VALUE:0-100;
 */
-(CGFloat) getCurrentFoodDemandValue;

@end
