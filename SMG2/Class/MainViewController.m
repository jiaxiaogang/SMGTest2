//
//  MainViewController.m
//  SMG2
//
//  Created by 贾  on 2017/3/15.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MainViewController.h"
#import "TransferTeacher.h"
#import "WordConcreteFactory.h"
#import "TempleteShadowWindow.h"
#import "VEWindowManager.h"
#import "SMG.h"
#import "WordConcrete.h"
#import "DB_MK.h"
#import "MKFood.h"
#import "LKDBHelper+Extension.h"
#import "NSObject+Extension.h"
#import "MKHeader.h"
#import "MKFruit.h"
#import "MemHeader.h"
#import "SMGLanguageHeader.h"

@interface MainViewController ()<UITextFieldDelegate,WordConcreteFactoryDelegate>

@property (weak, nonatomic) IBOutlet UITextField *questionTF;
@property (weak, nonatomic) IBOutlet UITextField *answerTF;
@property (strong,nonatomic) WordConcreteFactory *wordConcreteFactory;
@property (strong,nonatomic) NSString *curTransferTeacherName;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self initData];
    [self initDisplay];
}

-(void) initView{
    [self.answerTF setUserInteractionEnabled:false];
}

-(void) initData{
    self.wordConcreteFactory = [[WordConcreteFactory alloc] init];
}

-(void) initDisplay{
    self.wordConcreteFactory.delegate = self;
    self.questionTF.delegate = self;
    
    [self testMK];
    [self testMemory];
    [self testSMGLanguage];
}



/**
 *  MARK:--------------------method--------------------
 */
-(void) testMK{
    //1,初始化虚类表 和 抽象属性表
    [MKVirtualObject initializeForAllVirtuals];
    [MKAbstractObject initializeForAllAbstracts];
    
    //2,数据库
    DB_MK *db_MK = [DB_MK sharedInstance];
    
    //3,苹果
    MKFruit *fruit = [[MKFruit alloc] init];
    MKTaste *taste = [[MKTaste alloc] init];
    taste.sweet = 0.4;
    taste.sour = 0.5;
    fruit.mkTaste = taste;
    fruit.thisIs = NSStringFromClass([MKFruit class]);
    fruit.water = 0.6f;
    fruit.name = @"桃";
    fruit.productArea = @"北方";
    fruit.fromType = KnowledgeFromType_Editor;
    //[MKFruit insertToDB:fruit withDBHelper:db_MK];
    
    //4,存数据库
    [MKUtils insertSingleKnowledge:fruit];
    
    //5,查桃的味道
    [MKUtils searchSingleKnowledge:@"桃" withProperty:@"味道" withDB:db_MK withSuccess:^(MKObjectType type, id response) {
        NSLog(@"知识图谱中,桃的味道为:%@",[response getAllPropertysString]);
    } withError:^(NSString *errorCode) {
        NSLog(@"查找失败:%@",errorCode);
    }];
    
    //5,查桃的甜味
    [MKUtils searchSingleKnowledge:@"桃" withProperty:@"甜" withDB:db_MK withSuccess:^(MKObjectType type, id response) {
        NSLog(@"知识图谱中,桃的甜味为:%@",response);
    } withError:^(NSString *errorCode) {
        NSLog(@"查找失败:%@",errorCode);
    }];
}

-(void) testMemory{
    //我吃饭
    //1,我
    MKObject *iObj = [[MKObject alloc] init];
    [iObj setName:@"我"];
    [iObj setType:MKObjectType_Real];
    [iObj setFromType:KnowledgeFromType_Memory];
    [MKUtils insertSingleKnowledge:iObj];
    
    //2,饭
    MKObject *foodObj = [[MKObject alloc] init];
    [foodObj setName:@"桃"];
    [foodObj setType:MKObjectType_Real];
    [foodObj setFromType:KnowledgeFromType_Memory];
    [MKUtils insertSingleKnowledge:foodObj];
    
    //3,存行为记忆
    MemDoModel *doModel = [[MemDoModel alloc] init];
    doModel.fromMKId = iObj.rowid;
    doModel.toMKId = foodObj.rowid;
    doModel.type = DoType_Eat;
    [MemUtils insertSingleMemory:doModel withTimeCompareType:TimeCompareType_None];
    
    //4,查记忆
    [MemUtils searchSingleMemory:@"我" withDoType:DoType_Eat withToName:@"桃" withSuccess:^(id response) {
        NSLog(@"我吃过桃");
    } withError:^(NSString *errorCode) {
        NSLog(@"我没吃过!_!");
    }];
}

-(void) testSMGLanguage{
    //1,后天知识图谱-继承&实例
    GetVoiceCloudApi *api = [[GetVoiceCloudApi alloc] init];
    api.text = @"我是篮球。";
    [api startWithCompletionBlockWithSuccess:^(GetVoiceCloudApi *request, NSMutableArray *modelArr) {
        for (VoiceCloudModel *model in modelArr) {
            NSLog(@"词性标注:%@",model.pos);//可帮助人工智能极速学习语言基础
            NSLog(@"命名实体:%@",model.ne);//可用于知识图谱的建立(人名,地名,机构名)
            
            
        }
    }];
    
    //2,后天知识图谱-接口(技能力)
    GetVoiceCloudApi *api2 = [[GetVoiceCloudApi alloc] init];
    api2.text = @"我会打篮球。";
    [api2 startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (request.responseObject) {
            NSLog(@"%@",request.responseObject);
        }
    } failure:nil];
    
    //3,后天知识图谱-方法(行为)
    GetVoiceCloudApi *api3 = [[GetVoiceCloudApi alloc] init];
    api3.text = @"我在打篮球。";
    [api3 startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (request.responseObject) {
            NSLog(@"%@",request.responseObject);
        }
    } failure:nil];
    
    //4,后天知识图谱-属性
    GetVoiceCloudApi *api4 = [[GetVoiceCloudApi alloc] init];
    api4.text = @"我是圆的。";
    [api4 startWithCompletionBlockWithSuccess:nil failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (api4.responseString) {
            NSLog(@"%@",api4.responseString);
        }
    }];
}


/**
 *  MARK:--------------------onclick--------------------
 */
- (IBAction)smallChiOnClick:(id)sender {
    [self startTraining:@"smallChi"];
}

- (IBAction)smallBiOnClick:(id)sender {
    [self startTraining:@"smallBi"];
}

- (IBAction)smallShengOnClick:(id)sender {
    [self startTraining:@"smallSheng"];
}

//1,让WordConcreteFactory收集词汇信息
-(void) startTraining:(NSString*)name{
    self.curTransferTeacherName = name;
    [self.wordConcreteFactory startWord];
}

//2,把生成的wordConcrete交由调教师来告诉SMG
-(void) commitWordToSMG:(WordConcrete*)wordC{
    if (NSSTRINGISVALID(self.curTransferTeacherName)) {
        //3,SMG重新判断 词汇有效性 和 可信度等信息,
        //xxx
        //4,SMG_Dao收录词汇;
        TransferTeacher *tt = [[TransferTeacher alloc] initWithName:self.curTransferTeacherName];
        [tt trainingSMG:wordC];
    }
}

/**
 *  MARK:--------------------UITextFieldDelegate--------------------
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self.answerTF setText:@""];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSString *smgAnswer = [[SMG shareInstance] getHardness:textField.text];
    [self.answerTF setText:NSSTRINGTOISVALID(smgAnswer)];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.questionTF resignFirstResponder];
    return false;
}


/**
 *  MARK:--------------------WordConcreteFactoryDelegate--------------------
 */
-(void) wordConcreteFactory_Finish:(WordConcrete*)wordConcrete{
    [self commitWordToSMG:wordConcrete];//回答完毕,知识入脑
}
-(void) wordConcreteFactory_Failed:(NSString*)errorStr{
    NSLog(@"教学失败,哪个调教师教的?这都教不会?");
}
-(void) wordConcreteFactory_NextQuestion:(void(^)(NSString *answerStr))answerComplete withQuestion:(NSString*)question{
    TempleteShadowWindow *window = [[TempleteShadowWindow alloc] init];
    [window setFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [window setData:question withConfirmBlock:answerComplete];
    [VEWindowManager openWindow:window withParentView:self.view animation:true];
}





@end
