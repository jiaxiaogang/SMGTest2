//
//  XGUntil.h
//  SMG2
//
//  Created by 贾  on 2017/3/20.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XGUntil : NSObject

/**
 *  MARK:--------------------当前电量--------------------
 */
+(CGFloat) getBatteryValue;

@end
