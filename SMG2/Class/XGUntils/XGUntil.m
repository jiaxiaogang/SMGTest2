//
//  XGUntil.m
//  SMG2
//
//  Created by 贾  on 2017/3/20.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "XGUntil.h"

@implementation XGUntil

+(CGFloat) getBatteryValue{
    return [UIDevice currentDevice].batteryLevel;
}

@end
