//
//  CustomAbility.h
//  SMG2
//
//  Created by 贾  on 2017/3/20.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "AbilityBase.h"

/**
 *  MARK:--------------------自定义能力(后天能力)--------------------
 *  子类:语言,唱歌,画画等
 */
@interface CustomAbility : AbilityBase

@end
