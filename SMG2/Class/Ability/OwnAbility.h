//
//  OwnAbility.h
//  SMG2
//
//  Created by 贾  on 2017/3/20.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "AbilityBase.h"

/**
 *  MARK:--------------------自带能力(先天)--------------------
 *  子类:计算,摄像头,播放,麦克风,Input,Output,网络等;
 */
@interface OwnAbility : AbilityBase

@end
