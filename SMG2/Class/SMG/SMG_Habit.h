//
//  SMG_Habit.h
//  XGSmartHW
//
//  Created by 贾  on 2017/3/14.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  MARK:--------------------习惯--------------------
 *  用于管理"思维习惯"  (大脑处理过的任务记录)
 */
@interface SMG_Habit : NSObject

@end
