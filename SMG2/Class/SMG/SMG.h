//
//  SMG.h
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 *  MARK:--------------------智能小刚类--------------------
 *  需要:五味,五色,七音,五觉,欲望等;
 *
 *  先学习词汇,再有感觉;称为觉醒;(程序员执行)
 *  先创建感觉,再学习词汇;称为深造;(调教师执行)
 */
@class WordConcrete,TransferTeacher;
@interface SMG : NSObject

+(SMG*) shareInstance;
-(BOOL) checkValidWithWordConcrete:(WordConcrete*)wordC;
-(void) updateWordConcrete:(WordConcrete*)wordC withTransferTeacher:(TransferTeacher*)tt;
-(WordConcrete*) getWordConcreteWithWordText:(NSString*)wordText;
-(NSString*) getHardness:(NSString*)wordText;
@end
