//
//  Feel.h
//  SMG2
//
//  Created by 贾  on 2017/3/23.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  MARK:--------------------感觉基类--------------------
 *  功能说明:将感觉数字化
 *
 *  1,感觉目标
 *      1.1,感觉图像
 *      1.2,感觉人脸
 *      1.3,感觉实景
 *      1.4,感觉路线
 *      1.5,感觉一切
 *  2,感觉粒度:(由粗粒度向细粒度进化)
 *      1.1,图像粒度:将图中物体识别出并代码化其颜色,尺寸,形状等,再把里面物体找到*知识图谱索引*;
 *      1.2,人脸粒度:与大脸标准脸作对比;
 *          1.2.1,标准脸:(v1,男女各一套  v2,各年龄段,男女各一套)
 *      1.3,实景粒度:
 *  注:粗粒度是有模糊的粒度;(粒度值即模糊值)(模糊即范围)
 */
@interface Feel : NSObject

@end
