//
//  MKFruit.h
//  SMG2
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKFood.h"

/**
 *  MARK:--------------------水果父类表--------------------
 *  其子类如苹果:将使用知识图谱构建;不需要类;
 *
 */
@interface MKFruit : MKFood

@property (strong,nonatomic) NSString *productArea; //产地
+(void) initializeForVirtual;//仅运行一次

@end
