//
//  MKFood.m
//  SMG2
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKFood.h"

@implementation MKFood

+(void)initialize{
    [self setUserCalculateForCN:MKObjectId_ColumnName];
    [self setUserCalculateForCN:@"mkTaste"];//这里不写,进不到userGetValueForModel方法;
}

+(NSString *)getTableName{
    return NSStringFromClass([self class]);
}

+(NSDictionary *)getTableMapping{
    return nil;
}

-(id)userGetValueForModel:(LKDBProperty *)property
{
    if([property.propertyName isEqualToString:@"mkTaste"]){
        [MKTaste insertToDB:self.mkTaste withDBHelper:[DB_MK sharedInstance]];
        return NSStringFromClass([MKTaste class]);//把属性的类型名存到表上;到时候就去这里根据parentId找;
    }
    
    return [super userGetValueForModel:property];
}

-(void)userSetValueForModel:(LKDBProperty *)property value:(id)value
{
    [super userSetValueForModel:property value:value];
}

- (void)insert {}
- (void)delete {}
- (void)update {}
- (void)query {}


+(void) initializeForVirtual{
    MKVirtualObject *food = [[MKVirtualObject alloc] init];
    food.name = @"食物";
    food.type = MKObjectType_Virtual;
    food.className = NSStringFromClass([MKFood class]);
    //[MKVirtualObject insertToDB:food withDBHelper:[DB_MK sharedInstance]];
    [MKUtils insertSingleKnowledge:food];
}




@end
