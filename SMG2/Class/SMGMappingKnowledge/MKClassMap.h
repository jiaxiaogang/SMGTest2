//
//  MKClassMap.h
//  SMG2
//
//  Created by 贾  on 2017/3/29.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MKClassMap : NSObject

@property (assign,nonatomic) Class selfClass;
@property (assign,nonatomic) Class superClass;



/**
 *  MARK:--------------------init--------------------
 */
+(id) initWithSelfClass:(Class)selfClass;
+(id) initWithSelfClassName:(NSString*)selfClassName;

+(void)initializeAtFirstLaunch;


@end
