//
//  MKTaste.h
//  SMG2
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKHeader.h"

/**
 *  MARK:--------------------食物的味道--------------------
 *  是MKFood的属性;
 */
@interface MKTaste : MKObject

@property (assign, nonatomic) CGFloat sour;//酸0-1
@property (assign, nonatomic) CGFloat sweet;//甜0-1
@property (assign, nonatomic) CGFloat bitter;//苦0-1
@property (assign, nonatomic) CGFloat piquant;//辣0-1
@property (assign, nonatomic) CGFloat salty;//咸0-1


+(void) initializeForAbstract;//仅运行一次;

@end
