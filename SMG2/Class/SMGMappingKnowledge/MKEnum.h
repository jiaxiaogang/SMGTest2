//
//  MKEnum.h
//  SMG2
//
//  Created by 贾  on 2017/4/1.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

/**
 *  MARK:--------------------食物类别--------------------
 */
typedef NS_ENUM(NSInteger, FoodType) {
    FoodType_None           = 0,    //非食物
    FoodType_Vegetables     = 1,    //蔬菜
    FoodType_Fruits         = 2,    //水果
    FoodType_Meat           = 3,    //肉
};

/**
 *  MARK:--------------------知识图谱类别--------------------
 */
typedef NS_ENUM(NSInteger, MKObjectType) {
    MKObjectType_Real       = 0,    //实物
    MKObjectType_Virtual    = 1,    //类别
    MKObjectType_Abstract   = 2,    //属性
};

/**
 *  MARK:--------------------食物类别--------------------
 */
typedef NS_ENUM(NSInteger, KnowledgeFromType) {
    KnowledgeFromType_Editor        = 0,//先天编辑教的知识
    KnowledgeFromType_Memory        = 1,//后天记忆产生的知识
    KnowledgeFromType_SelfCreate    = 2,//后天自创产生的知识
};
