//
//  MKFruit.m
//  SMG2
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKFruit.h"

@implementation MKFruit

//在类 初始化的时候
+(void)initialize{
    [self setUserCalculateForCN:MKObjectId_ColumnName];
}

//表名
+(NSString *)getTableName{
    return NSStringFromClass([self class]);
}


+(NSDictionary *)getTableMapping{
    return nil;
}

-(id)userGetValueForModel:(LKDBProperty *)property{
    return [super userGetValueForModel:property];
}

-(void)userSetValueForModel:(LKDBProperty *)property value:(id)value{
    [super userSetValueForModel:property value:value];
}

- (void)insert {}
- (void)delete {}
- (void)update {}
- (void)query {}

+(void) initializeForVirtual{
    MKVirtualObject *food = [[MKVirtualObject alloc] init];
    food.name = @"水果";
    food.type = MKObjectType_Virtual;
    food.className = NSStringFromClass([MKFruit class]);
    //[MKVirtualObject insertToDB:food withDBHelper:[DB_MK sharedInstance]];
    [MKUtils insertSingleKnowledge:food];
}


@end
