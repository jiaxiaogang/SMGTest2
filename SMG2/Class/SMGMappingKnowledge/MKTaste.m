//
//  MKTaste.m
//  SMG2
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKTaste.h"

@implementation MKTaste

+(void)initialize{
    [self setUserCalculateForCN:MKObjectId_ColumnName];
}

+(NSString *)getTableName{
    return NSStringFromClass([self class]);
}

+(NSDictionary *)getTableMapping{
    return nil;
}

-(id)userGetValueForModel:(LKDBProperty *)property{
    return [super userGetValueForModel:property];
}

- (void)insert {}
- (void)delete {}
- (void)update {}
- (void)query {}


+(void) initializeForAbstract{
    //insert到属性表:"味道","酸","甜","苦","辣","咸" 6个词条;type2;
    MKVirtualObject *food = [[MKVirtualObject alloc] init];
    food.name = @"味道";
    food.type = MKObjectType_Virtual;
    food.className = NSStringFromClass([self class]);
    //[MKVirtualObject insertToDB:food withDBHelper:[DB_MK sharedInstance]];
    [MKUtils insertSingleKnowledge:food];
    
    
    NSArray *nameArr = @[@"酸",@"甜",@"苦",@"辣",@"咸"];
    NSArray *propertyNameArr = @[NSStringFromSelector(@selector(sour)),
                                 NSStringFromSelector(@selector(sweet)),
                                 NSStringFromSelector(@selector(bitter)),
                                 NSStringFromSelector(@selector(piquant)),
                                 NSStringFromSelector(@selector(salty))];
    for (int i = 0; i < nameArr.count; i ++) {
        NSString *name = nameArr[i];
        NSString *propertyName = propertyNameArr[i];
        MKAbstractObject *obj = [[MKAbstractObject alloc] init];
        obj.name = name;
        obj.type = MKObjectType_Abstract;
        obj.className = NSStringFromClass([self class]);
        obj.propertyName = propertyName;
        //[MKAbstractObject insertToDB:obj withDBHelper:[DB_MK sharedInstance]];
        [MKUtils insertSingleKnowledge:obj];
    }
}


@end
