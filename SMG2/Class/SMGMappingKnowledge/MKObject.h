//
//  MKObject.h
//  SMG2
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LKDBHelper.h"
#import "NSObject+Extension.h"
#import "DB_MK.h"
#import "DB_Memory.h"

/**
 *  MARK:--------------------知识图谱的基类--------------------
 *  MARK:--------------------知识图谱-映射表--------------------
 *  name,type
 */
@interface MKObject : NSObject

@property (strong,nonatomic) NSString *name;                //名称;(暂不考虑:外号)
@property (assign, nonatomic) MKObjectType type;            //物体类型
@property (assign, nonatomic) KnowledgeFromType fromType;   //知识来源

@end
