//
//  MKVirtualObject.h
//  SMG2
//
//  Created by 贾  on 2017/3/29.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKObject.h"

/**
 *  MARK:--------------------知识图谱项-虚物--------------------
 *  如:水果,东西
 */
@interface MKVirtualObject : MKObject

@property (strong,nonatomic) NSString *className;
+(void) initializeForAllVirtuals;//仅运行一次


@end
