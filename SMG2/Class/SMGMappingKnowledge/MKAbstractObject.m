//
//  MBAbstractObject.m
//  SMG2
//
//  Created by 贾  on 2017/3/29.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKAbstractObject.h"
#import "MKTaste.h"

@implementation MKAbstractObject


+(void)initialize{
    [self setUserCalculateForCN:MKObjectId_ColumnName];
}

+(NSString *)getTableName{
    return NSStringFromClass([self class]);
}

+(NSDictionary *)getTableMapping{
    return nil;
}

-(id)userGetValueForModel:(LKDBProperty *)property{
    return [super userGetValueForModel:property];
}

-(void)userSetValueForModel:(LKDBProperty *)property value:(id)value{
    [super userSetValueForModel:property value:value];
}


+(void) initializeForAllAbstracts{
    [MKTaste initializeForAbstract];
    
    
    
}

@end
