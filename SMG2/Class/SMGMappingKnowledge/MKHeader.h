//
//  MKHeader.h
//  SMG2
//
//  Created by 贾  on 2017/3/29.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKObject.h"
#import "MKRealObject.h"
#import "MKVirtualObject.h"
#import "MKAbstractObject.h"
#import "MKUtils.h"
#import "LKDBHelper+Extension.h"
#import "MKEnum.h"
#import "MemoryEnum.h"
#import "DBUtils.h"

#define MKObjectId_ColumnName   @"mkObjectId"//mkObjectId在其它表中的列名

#define MK_OTHER_ERROR          @"1000"//查询失败(其它错误)
#define MK_NO_FIND              @"1001"//未发现知识
#define MK_NO_PROPERTY          @"1002"//未发现指定的属性
#define MK_PARAMS_ERROR         @"1003"//提供的参数检查失败




