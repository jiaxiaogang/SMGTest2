//
//  MKUtils.h
//  SMG2
//
//  Created by 贾  on 2017/3/29.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>


@class MKObject,LKDBHelper;
@interface MKUtils : NSObject






/**
 *  MARK:--------------------获取自身的属性--------------------
 *	@param 	pronames 	保存属性名称
 *	@param 	protypes 	保存属性类型
 */
+ (void)getSelfPropertys:(NSMutableArray *)pronames protypes:(NSMutableArray *)protypes forClass:(Class)class;

//获取所有propertys
- (void)mutableString:(NSMutableString *)sb appendPropertyStringWithClass:(Class)clazz containParent:(BOOL)containParent;


/**
 *  MARK:--------------------递归收集InsertModel数组--------------------
 */
+(void) getInsertModelWithOutArr:(NSMutableArray*)outArr withModel:(MKObject*)model;



/**
 *  MARK:--------------------递归收集类继承图(子在前,父在后)--------------------
 */
+(NSMutableArray*) getClassLinkList:(Class)class;



/**
 *  MARK:--------------------学习一条知识--------------------
 */
+(void) insertSingleKnowledge:(MKObject*)model;



/**
 *  MARK:--------------------查询知识图谱简单的知识--------------------
 */
//某物属性
+(void) searchSingleKnowledge:(NSString*)objectName withProperty:(NSString*)property withDB:(LKDBHelper*)db withSuccess:(void(^)(MKObjectType type, id response))successBlock withError:(void(^)(NSString *errorCode))errorBlock;
//某物是否某物
+(BOOL) searchSingleKnowledge:(NSString*)objectName isName:(NSString*)isName;






@end
