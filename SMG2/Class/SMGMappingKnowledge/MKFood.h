//
//  MKFood.h
//  SMG2
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKHeader.h"
#import "MKTaste.h"

@interface MKFood : MKRealObject

@property (strong,nonatomic) MKTaste *mkTaste;
@property (assign, nonatomic) CGFloat water;    //含水量(0-1)

+(void) initializeForVirtual;//仅运行一次
@end
