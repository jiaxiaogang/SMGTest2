//
//  MKObject.m
//  SMG2
//
//  Created by 贾  on 2017/3/24.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKObject.h"
#import "MKHeader.h"

@implementation MKObject

//在类 初始化的时候(外键关联得时候必须实现)
+(void)initialize{
    //remove unwant property
    //比如 getTableMapping 返回nil 的时候   会取全部属性  这时候 就可以 用这个方法  移除掉 不要的属性
    //[self removePropertyWithColumnName:@"order"];
}

//表名
+(NSString *)getTableName{
    return NSStringFromClass([self class]);
}

- (void)insert {}
- (void)delete {}
- (void)update {}
- (void)query {}

-(id)userGetValueForModel:(LKDBProperty *)property{
    if([property.propertyName isEqualToString:MKObjectId_ColumnName]){
        return @([DB_MK sharedInstance].curMKObjectId);
    }
    return [super userGetValueForModel:property];
}

-(void)userSetValueForModel:(LKDBProperty *)property value:(id)value{
    [super userSetValueForModel:property value:value];
}


@end








////设置数据库数据(外键关联得时候必须实现)
//-(id)userGetValueForModel:(LKDBProperty *)property{
//    if([property.propertyName isEqualToString:@"mkTaste"]){
//        [MKTaste insertToDB:self.mkTaste withDBHelper:[DB_MK sharedInstance]];
//        return @(self.mkTaste.rowid);
//    }
//
//    if([property.propertyName isEqualToString:MKObjectId_ColumnName]){
//        MKObject *mkObj = [[MKObject alloc] init];
//        mkObj.names = self.names;
//        [MKObject insertToDB:mkObj withDBHelper:[DB_MK sharedInstance]];
//        return @(mkObj.rowid);
//    }
//    return nil;
//}

////取数据库数据(外键关联得时候必须实现)
//-(void)userSetValueForModel:(LKDBProperty *)property value:(id)value
//{
//    if([property.propertyName isEqualToString:@"mkTaste"]){
//        self.mkTaste = nil;
//        //当ID为int的时候 ID= %d  不需要单引号
//        NSArray *arr = [NSObject searchWithWhere:[NSString stringWithFormat:@"rowid='%@'",value] orderBy:nil offset:0 count:1 withDBHelper:[DB_MK sharedInstance]];
//        if (arr.count > 0) {
//            self.mkTaste = arr[0];
//        }
//    }
//
//    if([property.propertyName isEqualToString:MKObjectId_ColumnName]){
//        self.names = [MKObject searchSingleWithWhere:[NSString stringWithFormat:@"rowid='%@'",value] orderBy:nil withDBHelper:[DB_MK sharedInstance]];
//    }
//}

////更新表结构
//+(void)dbDidAlterTable:(LKDBHelper *)helper tableName:(NSString *)tableName addColumns:(NSArray *)columns
//{
//    for (int i=0; i<columns.count; i++)
//    {
//        LKDBProperty* p = [columns objectAtIndex:i];
//        if([p.propertyName isEqualToString:@"error"])
//        {
//            [helper executeDB:^(FMDatabase *db) {
//                NSString* sql = [NSString stringWithFormat:@"update %@ set error = name",tableName];
//                [db executeUpdate:sql];
//            }];
//        }
//    }
//}
//
////设置列属性（可选）
//+(void)columnAttributeWithProperty:(LKDBProperty *)property
//{
//    if([property.sqlColumnName isEqualToString:@"MyAge"])
//    {
//        property.defaultValue = @"15";
//    }
//    if([property.propertyName isEqualToString:@"date"])
//    {
//        property.isUnique = YES;
//        property.checkValue = @"MyDate > '2000-01-01 00:00:00'";
//        property.length = 30;
//    }
//}
