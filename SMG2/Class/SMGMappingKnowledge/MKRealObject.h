//
//  MKRealObject.h
//  SMG2
//
//  Created by 贾  on 2017/3/29.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKObject.h"

/**
 *  MARK:--------------------知识图谱项-实物表--------------------
 *  如:苹果
 */
@interface MKRealObject : MKObject

@property (assign, nonatomic) CGFloat trustValue;   //总可信度
@property (strong,nonatomic) NSString *thisIs;      //指向此实物的父类;

@end
