//
//  MBAbstractObject.h
//  SMG2
//
//  Created by 贾  on 2017/3/29.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKObject.h"

/**
 *  MARK:--------------------知识图谱项-抽象--------------------
 *  如:味道,速度
 */
@interface MKAbstractObject : MKObject

@property (strong,nonatomic) NSString *className;
@property (strong,nonatomic) NSString *propertyName;
+(void) initializeForAllAbstracts;

@end
