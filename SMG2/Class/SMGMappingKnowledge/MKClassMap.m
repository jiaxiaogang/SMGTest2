//
//  MKClassMap.m
//  SMG2
//
//  Created by 贾  on 2017/3/29.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MKClassMap.h"
#import "MKHeader.h"
#import "MKFruit.h"
#import "MKTaste.h"

@implementation MKClassMap


+(void)initialize{
    
}

+(NSString *)getTableName{
    return NSStringFromClass([self class]);
}

+(NSDictionary *)getTableMapping{
    return nil;
}

- (void)insert {}
- (void)delete {}
- (void)update {}
- (void)query {}

/**
 *  MARK:--------------------init--------------------
 */
+(id) initWithSelfClass:(Class)selfClass{
    if (selfClass && [selfClass superclass]) {
        MKClassMap *map = [[MKClassMap alloc] init];
        map.selfClass = selfClass;
        map.superClass = [selfClass superclass];
        return map;
    }
    return nil;
}
+(id) initWithSelfClassName:(NSString*)selfClassName{
    Class selfClass = NSClassFromString(selfClassName);
    return [MKClassMap initWithSelfClass:selfClass];
}

+(void)initializeAtFirstLaunch{
    
    NSArray *arr = @[[MKObject class],[MKRealObject class],[MKVirtualObject class],[MKAbstractObject class],[MKTaste class],[MKFood class],[MKFruit class]];
    //MKObject insertToDBWithArray:<#(nonnull NSArray *)#> filter:<#^(id  _Nonnull model, BOOL inserted, BOOL * _Nullable rollback)filter#> completed:<#^(BOOL allInserted)completedBlock#>
}


@end
