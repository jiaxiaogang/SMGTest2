//
//  MemUtils.h
//  SMG2
//
//  Created by 贾  on 2017/4/1.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MemHeader.h"

@interface MemUtils : NSObject


/**
 *  MARK:--------------------插入一条记忆--------------------
 */
+(void) insertSingleMemory:(MemDoModel*)doModel withTimeCompareType:(TimeCompareType)timeCompareType ;


/**
 *  MARK:--------------------查询一条普通记忆--------------------
 */
+(void) searchSingleMemory:(NSString*)fromName withDoType:(DoType)doType withToName:(NSString*)toName withSuccess:(void(^)(id response))successBlock withError:(void(^)(NSString *errorCode))errorBlock;

@end
