//
//  MemoryEnum.h
//  SMG2
//
//  Created by 贾  on 2017/4/1.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

/**
 *  MARK:--------------------行为类型--------------------
 */
typedef NS_ENUM(NSInteger, DoType) {
    DoType_Unknown      = 0,//未知
    DoType_HitTo        = 1,//打_
    DoType_HitEach      = 2,//打_互相
    DoType_HitUnknown   = 3,//打_未知
    DoType_Eat          = 4,//吃
    DoType_SeeTo        = 5,//看
    DoType_At           = 6,//在某地
    DoType_BeenTo       = 7,//到过某地,去过某地
    DoType_To           = 8,//从x到y;
    DoType_Time         = 9,//我在x时间;
};

/**
 *  MARK:--------------------行为类型--------------------
 */
typedef NS_ENUM(NSInteger, TimeCompareType) {
    TimeCompareType_None        = 0,//未知
    TimeCompareType_Front       = 1,//早
    TimeCompareType_Same        = 2,//同时
    TimeCompareType_Back        = 3,//迟
};
