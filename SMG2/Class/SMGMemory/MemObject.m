//
//  MemObject.m
//  SMG2
//
//  Created by 贾  on 2017/4/1.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MemObject.h"
#import "MemHeader.h"

@implementation MemObject

+ (BOOL)insertToMemory:(NSObject *)model{
    if ([model isMemberOfClass:self]){
        return [[DB_Memory sharedInstance] insertToDB:model];
    }
    return NO;
}

+ (NSMutableArray *)searchMemoryWithWhere:(id)where orderBy:(NSString *)orderBy offset:(NSInteger)offset count:(NSInteger)count{
    return [[DB_Memory sharedInstance] search:self where:where orderBy:orderBy offset:offset count:count];
}

+ (id)searchSingleMemoryWithWhere:(id)where{
    return [[DB_Memory sharedInstance] searchSingle:self where:where orderBy:nil];
}

@end
