//
//  MemFeedModel.h
//  SMG2
//
//  Created by 贾  on 2017/4/5.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MemObject.h"

/**
 *  MARK:--------------------记忆关系表--------------------
 *  1,包含关系用树形结构;1分2分4分8;
 *      (使用parentFeedId来表示)
 *  2,先后关系用链式结构;1到2到3到4;
 */
@interface MemFeedModel : MemObject

@property (assign, nonatomic) NSInteger lastDoId;                   //上一条行为lastDoId
@property (assign, nonatomic) NSInteger doId;                       //行为doId
@property (assign, nonatomic) NSInteger parentFeedId;               //父记忆parentFeedId
@property (assign, nonatomic) TimeCompareType timeCompareType;      //两个行为发生的先后关系

@end
