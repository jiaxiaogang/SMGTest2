//
//  MemHeader.h
//  SMG2
//
//  Created by 贾  on 2017/4/1.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MemObject.h"
#import "DB_Memory.h"
#import "NSObject+Extension.h"
#import "DBUtils.h"
#import "MemDoModel.h"
#import "MemFeedModel.h"
#import "MemUtils.h"


#define MEM_NO_FIND              @"1001"//未发现记忆
#define MEM_PARAMS_ERROR         @"1002"//提供的参数检查失败



//是否空字符串
#define STRISOK(a) (a  && ![a isKindOfClass:[NSNull class]] && [a isKindOfClass:[NSString class]] && ![a isEqualToString:@""])

//字符串防闪
#define STRTOOK(a) (a  && ![a isKindOfClass:[NSNull class]]) ? ([a isKindOfClass:[NSString class]] ? a : [NSString stringWithFormat:@"%@", a]) : @""

//数组防闪
#define ARRTOOK(a) (a  && [a isKindOfClass:[NSArray class]]) ?  a : [NSArray new]

//数组取子防闪
#define ARR_INDEX(a,i) (a && [a isKindOfClass:[NSArray class]] && a.count > i) ?  a[i] : nil
