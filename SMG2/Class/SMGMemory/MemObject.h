//
//  MemObject.h
//  SMG2
//
//  Created by 贾  on 2017/4/1.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MemObject : NSObject


+ (BOOL)insertToMemory:(NSObject *)model;

+ (NSMutableArray *)searchMemoryWithWhere:(id)where orderBy:(NSString *)orderBy offset:(NSInteger)offset count:(NSInteger)count;

+ (id)searchSingleMemoryWithWhere:(id)where;

@end
