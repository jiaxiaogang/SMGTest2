//
//  MemDoModel.h
//  SMG2
//
//  Created by 贾  on 2017/4/5.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "MemObject.h"

/**
 *  MARK:--------------------行为表--------------------
 *  参考:2017.04.04记忆架构分析图
 */
@interface MemDoModel : MemObject

@property (assign, nonatomic) DoType type;
@property (assign, nonatomic) NSInteger fromMKId;
@property (assign, nonatomic) NSInteger toMKId;

@end
