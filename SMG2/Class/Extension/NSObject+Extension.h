//
//  NSObject+Extension.h
//  SMG2
//
//  Created by 贾  on 2017/3/28.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+LKDBHelper.h"

/**
 *  MARK:--------------------LKDB--------------------
 */
@interface NSObject (LKDB)

+ (BOOL)insertToDB:(NSObject *)model withDBHelper:(LKDBHelper*)dbHelper;
+ (NSMutableArray *)searchWithWhere:(id)where orderBy:(NSString *)orderBy offset:(NSInteger)offset count:(NSInteger)count withDBHelper:(LKDBHelper*)dbHelper;
+ (id)searchSingleWithWhere:(id)where orderBy:(NSString *)orderBy withDBHelper:(LKDBHelper*)dbHelper;

@end
