//
//  NSObject+Extension.m
//  SMG2
//
//  Created by 贾  on 2017/3/28.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "NSObject+Extension.h"


@implementation NSObject (LKDB)

+ (BOOL)insertToDB:(NSObject *)model withDBHelper:(LKDBHelper*)dbHelper
{
    
    if ([model isMemberOfClass:self]){
        if (dbHelper){
            return [dbHelper insertToDB:model];
        }
    }
    return NO;
}

+ (NSMutableArray *)searchWithWhere:(id)where orderBy:(NSString *)orderBy offset:(NSInteger)offset count:(NSInteger)count withDBHelper:(LKDBHelper*)dbHelper
{
    if (dbHelper) {
        return [dbHelper search:self where:where orderBy:orderBy offset:offset count:count];
    }
    return nil;
}

+ (id)searchSingleWithWhere:(id)where orderBy:(NSString *)orderBy withDBHelper:(LKDBHelper*)dbHelper
{
    if (dbHelper) {
        return [dbHelper searchSingle:self where:where orderBy:orderBy];
    }
    return nil;
    
}
@end
