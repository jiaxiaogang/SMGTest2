//
//  AppDelegate.h
//  SMG2
//
//  Created by 贾  on 2017/3/15.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) MainViewController *mainPage;

@end

